import React, { useState } from 'react';
import ModalPopup from './ModalPopup';

const MyButton = () => {

    const [modalShow, setModalShow] = useState(false);

    const handleClickMenuItem = () => {
        setModalShow(true);
    }

    return (
        <div>
            <form>
                <input type="submit" value="button" onClick={() => handleClickMenuItem()} />
            </form>
            <ModalPopup
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
};

export default MyButton;