import axios from 'axios';
import React,{useState, Component, useCallback} from 'react'; 
import {useDropzone} from 'react-dropzone';
import { Button } from 'react-bootstrap';
import './selectFileToUpload.scss';

const SelectFileToUpload = (props) => {

  const [selectedFile, setSelectedFile] = useState(null);

  const onDrop = useCallback(acceptedFiles => {
    console.log(acceptedFiles);
    setSelectedFile(acceptedFiles[0]);
  }, []);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    onDrop
  });

    const onFileChange = event => { 
      // Update the state 
      setSelectedFile(event.target.files[0]);
      // this.setState({ selectedFile: event.target.files[0] }); 
    }; 
     
    // On file upload (click the upload button) 
    const onFileUpload = () => { 
      // Create an object of formData 
      const formData = new FormData(); 
     
      //append expected data whch has to be sent in a request body as expected by backend
      formData.append('file', selectedFile);
      formData.append('parentWorkspaceId', 5642865);
    //   formData.append('parentWorkspaceId', props.currentworkspaceid);
    
      // Details of the uploaded file 
      axios({
        method: 'post',
        withCredentials: true,
        url: 'api-link',
        data: formData  
      }).then(response => {
        alert(response.data);
      }, (error) => {
        console.log(error);
        alert('Please select file and try..');
      });
    }; 
    
    // File content to be displayed after 
    // file upload is complete 
    const fileData = () => { 
      if (selectedFile) { 
          
        return ( 
          <div> 
            <p>File Name: {selectedFile.name}</p> 
          </div> 
        ); 
       } 
    }; 
     
      return ( 
        <>
        <div className='file-upload-box'>
          <div className='drag-drop' {...getRootProps()}>
            <input {...getInputProps()} />
            <p className="drop-here">+</p>
            <p className="drop-here-msg">Drag and Drop your Knowledge Asset here to upload</p>
          </div>
          {/* <hr className='border'></hr> */}
          <span className='line'>
            <h2><span>OR</span></h2>
          </span>
          <div className='select-file'>
            <div> 
                <label htmlFor="file-upload" className="custom-file-upload">
                    <i className="fa fa-cloud-upload"></i> Browse file
                </label>
                <input id="file-upload" type="file" onChange={onFileChange} />
            </div> 
            {fileData()} 
          </div>  
          <div className='uploadAsset'> 
            <Button variant="primary" className='upload-button' onClick={onFileUpload}> 
              Add Knowledge asset
            </Button> 
          </div>
        </div>
        </>
         
      ); 
    } 
  
  export default SelectFileToUpload;