import React from 'react'
import SelectFileToUpload from './SelectFileToUpload';
import './AddNew.scss';

const AddNew = () => {
    return (
        <>
            <div className="file-Upload-container">
                <div className='upload_body'>
                    <SelectFileToUpload />                    
                </div>
            </div>
        </>
    )
}

export default AddNew;