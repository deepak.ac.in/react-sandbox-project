import React from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import './ModalPopup.scss';
import Sidebar from './Sidebar';

function ModalPopup(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                Add a knowlwdge assets
            </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Sidebar />
                {/* <p>Hi This is all to go. Hi This is all to go. Hii This is all to go. Hi This is all to go. Hi This is all to go.</p>
                <h1>ahhaaaaa</h1> */}
            </Modal.Body>
        </Modal>
    );
}

export default ModalPopup;