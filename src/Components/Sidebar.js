import React, { useState, useEffect, useRef } from "react";
import { Link, NavLink, useLocation } from 'react-router-dom';
import './Sidebar.scss';
import AddNew from './AddNew';
import SelectExisting from './SelectExisting';
import CreateOnline from './CreateOnline';
import CloneOnline from './CloneExisting';

const sidebarNavItems = [
    {
        title: 'Add new',
        path: '/add-new',
        cName: 'nav-text'
    },
    {
        title: 'Create online',
        path: '/create-online',
        cName: 'nav-text'
    },
    {
        title: 'Select existing',
        path: '/select-existing',
        cName: 'nav-text'
    },
    {
        title: 'Clone existing',
        path: '/clone-existing',
        cName: 'nav-text'
    },
]

const Sidebar = () => {
    const [sidebar, setSidebar] = useState(false);
    const [showResults, setShowResults] = React.useState(false)
    const [clickedIndex, setClickedIndex] = useState(0);

    const handleClickMenuItem = (clickedIndex) => {
      setShowResults(true);
      setClickedIndex(clickedIndex);
    }

    const Results = () => {
        return(
          <div id="results" className="search-results">
            {/* this is ....{sidebarNavItems[clickedIndex].title} */}
            {(() => {
              switch (clickedIndex) {
                case 0:
                  return <AddNew />
                case 1:
                  return <CreateOnline />
                case 2:
                  return <SelectExisting />
                case 3:
                  return <CloneOnline />
                default:
                  return null
              }
            })()}
          </div>
        );
    }
  
    return (
      <>
        <div className="popup-container">
          <div className="popup-nav">
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
              <ul className='nav-menu-items'>
                {sidebarNavItems.map((item, index) => {
                  return (
                    <li key={index} className={item.cName} onClick={() => handleClickMenuItem(index)}>
                      <NavLink className="nav-link" to={'#'}>
                        <span>{item.title}</span>
                      </NavLink>
                    </li>
                  );
                })}
              </ul>
            </nav>
          </div>
          <div className="popup-root" id="root">
              {/*  This element's contents will be replaced with your component.  */}
                { showResults ? <Results /> : null }        
          </div>
        </div>
      </>
    );
};

export default Sidebar;