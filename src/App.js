import { HashRouter as Router, Routes, Route, Link } from "react-router-dom";
import MyButton from "../src/Components/MyButton";

import './App.css';

function App() {
  return (
    <Router>
            {
              <Routes>
                  <Route exact path="/MyButton" element={<MyButton />} />
                  <Route exact path="/" element={<span><Link to="/MyButton">Explore Workspace</Link></span>} />
              </Routes>
            }
        </Router>
  );
}

export default App;
